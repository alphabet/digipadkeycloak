# Check out https://hub.docker.com/_/node to select a new base image
FROM hub.eole.education/proxyhub/library/node:16-alpine

# update and install dependency
RUN apk update && apk upgrade

# Set to a non-root built-in user `node`
USER node

# Create app directory (with user `node`)
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

# Install app dependencies
COPY --chown=node package*.json ./
# Bundle app source code
COPY --chown=node . .

RUN npm install
RUN npm run build

# expose app port on container
EXPOSE 3000

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=3000

CMD [ "npm", "start" ]
